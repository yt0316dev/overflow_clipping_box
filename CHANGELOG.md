## 2.0.2

- Optimize

## 2.0.1

- performLayout: Small optimization
- example: Fix logo tile size

## 2.0.0

- Use RenderAligningShiftedBox
- Rename: width -> minWidth, height -> minHeight

## 1.0.0

- Initial release
