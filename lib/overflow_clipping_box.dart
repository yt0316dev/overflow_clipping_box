import 'dart:math';

import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

@immutable
class OverflowClippingBox extends SingleChildRenderObjectWidget {
  const OverflowClippingBox({
    required Widget child,
    this.minWidth = 240,
    this.minHeight = 240,
    this.textDirection,
    this.alignment = Alignment.topLeft,
    Key? key,
  }) : super(key: key, child: child);

  final double minWidth;
  final double minHeight;
  final TextDirection? textDirection;
  final AlignmentGeometry alignment;

  @override
  RenderObject createRenderObject(BuildContext context) {
    return _RenderOverflowClippingBox(
      minWidth: minWidth,
      minHeight: minHeight,
      textDirection: textDirection,
      alignment: alignment,
    );
  }

  @override
  void updateRenderObject(
    BuildContext context,
    _RenderOverflowClippingBox renderObject,
  ) {
    renderObject
      ..minWidth = minWidth
      ..minHeight = minHeight
      ..textDirection = textDirection
      ..alignment = alignment;
  }
}

class _RenderOverflowClippingBox extends RenderAligningShiftedBox {
  _RenderOverflowClippingBox({
    required double minWidth,
    required double minHeight,
    required TextDirection? textDirection,
    required AlignmentGeometry alignment,
  })  : _minWidth = minWidth,
        _minHeight = minHeight,
        super(textDirection: textDirection, alignment: alignment);

  double _minWidth;
  double get minWidth => _minWidth;
  set minWidth(double newValue) {
    if (_minWidth == newValue) {
      return;
    }
    _minWidth = newValue;
    markNeedsLayout();
  }

  double _minHeight;
  double get minHeight => _minHeight;
  set minHeight(double newValue) {
    if (_minHeight == newValue) {
      return;
    }
    _minHeight = newValue;
    markNeedsLayout();
  }

  @override
  void performLayout() {
    final child = this.child;
    if (child == null) {
      return super.performLayout();
    }

    child.layout(
      BoxConstraints.tightFor(
        width: max(minWidth, constraints.maxWidth),
        height: max(minHeight, constraints.maxHeight),
      ),
      parentUsesSize: true,
    );
    size = constraints.biggest;
    alignChild();
  }

  final _clipRectLayer = LayerHandle<ClipRectLayer>();

  @override
  void paint(PaintingContext context, Offset offset) {
    final child = this.child;
    if (child != null) {
      _clipRectLayer.layer = context.pushClipRect(
        needsCompositing,
        offset,
        Offset.zero & size,
        super.paint,
        oldLayer: _clipRectLayer.layer,
      );
    }
  }
}
