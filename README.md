# overflow_clipping_box

## Usage

```dart
void main() {
  runApp(
    const OverflowClippingBox(
      child: _App(),
    ),
  );
}
```

## Default width and height

Default width and height (240) are based on:

```
320dp: a typical phone screen (240x320 ldpi, 320x480 mdpi, 480x800 hdpi, etc).
```

https://developer.android.com/guide/topics/large-screens/support-different-screen-sizes?hl=en
