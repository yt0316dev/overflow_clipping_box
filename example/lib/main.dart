import 'package:flutter/material.dart';
import 'package:overflow_clipping_box/overflow_clipping_box.dart';

void main() {
  runApp(
    const OverflowClippingBox(
      minHeight: 288,
      child: _App(),
    ),
  );
}

@immutable
class _App extends StatelessWidget {
  const _App({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: _Scaffold(),
    );
  }
}

const _logoTileSize = 64.0;
const _logoSize = _logoTileSize * 3 + 4 * 2;

@immutable
class _Scaffold extends StatelessWidget {
  const _Scaffold({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: const Center(
        child: SizedBox(
          width: _logoSize,
          height: _logoSize,
          child: OverflowClippingBox(
            minWidth: _logoSize,
            minHeight: _logoSize,
            alignment: Alignment.center,
            child: _Logo(),
          ),
        ),
      ),
    );
  }
}

@immutable
class _Logo extends StatelessWidget {
  const _Logo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 4,
      runSpacing: 4,
      children: const [
        _LogoTile(alignment: Alignment.topLeft),
        _LogoTile(alignment: Alignment.topCenter),
        _LogoTile(alignment: Alignment.topRight),
        _LogoTile(alignment: Alignment.centerLeft),
        _LogoTile(alignment: Alignment.center),
        _LogoTile(alignment: Alignment.centerRight),
        _LogoTile(alignment: Alignment.bottomLeft),
        _LogoTile(alignment: Alignment.bottomCenter),
        _LogoTile(alignment: Alignment.bottomRight),
      ],
    );
  }
}

@immutable
class _LogoTile extends StatelessWidget {
  const _LogoTile({
    required this.alignment,
    Key? key,
  }) : super(key: key);

  final AlignmentGeometry alignment;

  @override
  Widget build(BuildContext context) {
    return Container(
      foregroundDecoration: BoxDecoration(border: Border.all()),
      width: _logoTileSize,
      height: _logoTileSize,
      child: OverflowClippingBox(
        minWidth: _logoSize,
        minHeight: _logoSize,
        alignment: alignment,
        child: const FlutterLogo(),
      ),
    );
  }
}
